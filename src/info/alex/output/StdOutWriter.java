package info.alex.output;

import java.io.PrintStream;

public class StdOutWriter {
    private PrintStream out;

    public StdOutWriter(PrintStream out) {
        this.out = out;
    }

    public void write(String str) {
        out.println(str);
    }
}
