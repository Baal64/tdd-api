package info.alex.test.input;

import org.junit.Test;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class KeyboardInputTest {

    @Test
    public void canReadKeybordInputFromStdIn() {

        String string = "livre";

        InputStream inputStream = new ByteArrayInputStream(string.getBytes(Charset.forName("UTF-8")));
        Scanner input = new Scanner(inputStream);
        String str = input.next();

        assertEquals("livre", str);
    }
}
