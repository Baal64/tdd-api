package info.alex.test.input;

import info.alex.output.StdOutWriter;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

class OutputTest {

    @Test
    public void canWriteTextresult() throws FileNotFoundException {

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        StdOutWriter writer = new StdOutWriter(new PrintStream(String.valueOf(outputStream)));
        writer.write("livre");

        assertEquals("livre", outputStream.toString());

    }
}
