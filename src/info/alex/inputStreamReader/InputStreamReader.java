package info.alex.inputStreamReader;

import java.io.InputStream;
import java.util.Scanner;

public class InputStreamReader {

    private InputStream in;

    public InputStreamReader(InputStream in) {
        this.in = in;
    }

    public String readText(){
        final Scanner input = new Scanner(in);
        System.out.println("Veuillez saisir un titre :");
        final String str = input.nextLine();
        return str;
    }
}
