package info.alex;

import info.alex.inputStreamReader.InputStreamReader;
import info.alex.output.StdOutWriter;

public class Main {

    public static void main(String[] args) {
        InputStreamReader input = new InputStreamReader(System.in);

        String entree = input.readText();

        StdOutWriter output = new StdOutWriter(System.out);
        output.write(entree);

    }
}
